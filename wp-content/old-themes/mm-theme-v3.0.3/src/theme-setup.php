<?php
/**
 * Theme setup.
 *
 * @package mm
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function mm_theme_setup() {

	load_theme_textdomain(THEME_NAME, THEME_DIR . '/languages');

	/**
	 * Load editor style.
	 */
	add_editor_style();

	/**
	 * Enable support for Post Thumbnails.
	 */
	add_theme_support('post-thumbnails');

	/**
	 * Enable support for Post Formats.
	 */
	//add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status' ) );
	
	/**
	 * Enable support for menus.
	 */
	add_theme_support('widgets');

	/**
	 * Enable support for widgets.
	 */
	add_theme_support('widgets');

	/**
	 * Register custom menu.
	 */
	register_nav_menus( array(
		'primary'     => _x( 'Primary Menu', 'backend', THEME_NAME ),
		'mobile'     => _x( 'Mobile Menu', 'backend', THEME_NAME ),
		'footer'      => _x( 'Footer Menu', 'backend', THEME_NAME ),
		'footer_orange_box'      => _x( 'Footer organe box', 'backend', THEME_NAME ),
	) );

	/**
	 * Additional image size.
	 */
	add_image_size('thumb-235x160', 235, 160, true);
	add_image_size('thumb-370x200', 370, 200, true);
	add_image_size('thumb-770x400', 770, 400, true);
	add_image_size('thumb-900x500', 900, 500, true);
	add_image_size('intro-1170x440', 1170, 440, true);
	add_image_size('post-box', 1170, 400, true);
	add_image_size('post-box-small', 585, 400, true);
	add_image_size('full_hd', 1920, 1080, true);
	//add_image_size('thumb-450x350', 450, 300, true);
	//add_image_size('thumb-600x420', 600, 420, true);
	//add_image_size('thumb-800x400', 800, 400, false);

	/**
	 * Enable crop.
	 */
	if(!get_option('medium_crop')) {
		update_option('medium_crop', '1');
	}
}
add_action('after_setup_theme', 'mm_theme_setup', 10);

/**
 * Register widgetized areas.
 */
function mm_widgets_init() {

	$params = array(
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	);
	
	$areas = array(
		'sidebar_default' => array(
			'name' => __( 'Sidebar: Domyślny', THEME_NAME ),
			'desc' => __( 'Widgety widoczne w panelu bocznym domyślnie.', THEME_NAME ),
		),
		'sidebar_home' => array(
			'name' => __( 'Sidebar: Strona główna', THEME_NAME ),
			'desc' => __( 'Widgety widoczne w panelu bocznym na stronie głównej.', THEME_NAME ),
		),
		'sidebar_category' => array(
			'name' => __( 'Sidebar: Kategoria', THEME_NAME ),
			'desc' => __( 'Widgety widoczne w panelu bocznym w kategorii.', THEME_NAME ),
		),
		'sidebar_single' => array(
			'name' => __( 'Sidebar: Wpis', THEME_NAME ),
			'desc' => __( 'Widgety widoczne w panelu bocznym we wpisie.', THEME_NAME ),
		),
		'footer_1' => array(
			'name' => __( 'Footer 1', THEME_NAME ),
			'desc' => __( 'Stopka', THEME_NAME ),
			'before_title' => '<h5>',
			'after_title' => '</h5>',
		),
		'footer_2' => array(
			'name' => __( 'Footer 2', THEME_NAME ),
			'desc' => __( 'Stopka', THEME_NAME ),
			'before_title' => '<h5>',
			'after_title' => '</h5>',
		),
		'footer_3' => array(
			'name' => __( 'Footer 3', THEME_NAME ),
			'desc' => __( 'Stopka', THEME_NAME ),
			'before_title' => '<h5>',
			'after_title' => '</h5>',
		),
		'footer_4' => array(
			'name' => __( 'Footer 4', THEME_NAME ),
			'desc' => __( 'Stopka', THEME_NAME ),
			'before_title' => '<h5>',
			'after_title' => '</h5>',
		),
		'footer_5' => array(
			'name' => __( 'Footer 5', THEME_NAME ),
			'desc' => __( 'Stopka', THEME_NAME ),
			'before_title' => '<h5>',
			'after_title' => '</h5>',
		),
		'sidebar_contact' => array(
			'name' => __( 'sidebar: Kontakt', THEME_NAME ),
			'desc' => __( 'Widgety widoczne w panelu bocznym domyślnej strony', THEME_NAME ),
			'before_title' => '<h5>',
			'after_title' => '</h5>',
		),
	);

	if ( ! empty( $areas ) && is_array( $areas ) ) {
		$prefix = '';

		foreach( $areas as $sidebar_id=>$sidebar ) {
			if($sidebar['before_widget']) $params['before_widget'] = $sidebar['before_widget'];
			if($sidebar['after_widget']) $params['after_widget'] = $sidebar['after_widget'];
			if($sidebar['before_title']) $params['before_title'] = $sidebar['before_title'];
			if($sidebar['after_title']) $params['after_title'] = $sidebar['after_title'];
			$sidebar_args = array(
				'name'          => ( isset( $sidebar['name'] ) ? $sidebar['name'] : '' ),
				'id'            => $prefix . $sidebar_id,
				'description'   => ( isset( $sidebar['desc'] ) ? $sidebar['desc'] : '' ),
				'before_widget' => $params['before_widget'],
				'after_widget'  => $params['after_widget'],
				'before_title'  => $params['before_title'],
				'after_title'   => $params['after_title'],
			);
			register_sidebar( $sidebar_args );
		}
	}
}
add_action( 'widgets_init', 'mm_widgets_init' );

/**
 * Register scripts and styles.
 */
function mm_enqueue_scripts() {

	//wp_enqueue_style('mm-assets-style', THEME_URI.'/assets/css/style.css', array(), '1.0.0');

	wp_enqueue_style('mm-assets-style', THEME_URI.'/assets/css/style.css', array(), '1.0.0');

	wp_enqueue_style('mm-style', get_stylesheet_uri(), array(), '1.0.0');


	wp_enqueue_script('mm-vendor', THEME_URI.'/assets/js/vendor.js', array(), '1.0.0', true);

	wp_enqueue_script('mm-app', THEME_URI.'/assets/js/app.js', array(), '1.0.0', true);

	wp_enqueue_script('mm-wp', THEME_URI.'/wp.js', array(), '1.0.0', true);

}
add_action('wp_enqueue_scripts', 'mm_enqueue_scripts');

/**
 * Excerpt settings
 */
function mm_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_more', 'mm_excerpt_more');

function mm_excerpt_length($length) {
	return 30;
}
add_filter('excerpt_length', 'mm_excerpt_length', 999);

/**
 * Remove default gallery styling
 */
add_filter('use_default_gallery_style', '__return_false');

/**
 * Add TinyMCE style
 */
function mm_mce_config($init) {
	$style_formats = array (
		array('title' => 'Lead', 'inline' => 'span', 'classes' => 'lead')
	);
	$init['style_formats'] = json_encode($style_formats);
	$init['style_formats_merge'] = false;
	
	return $init;
}
add_filter('tiny_mce_before_init', 'mm_mce_config');

/**
 * Custom preloader for CF7
 */
// function mm_wpcf7_ajax_loader() {
// 	return get_template_directory_uri().'/assets/images/preloader.gif';
// }
// add_filter('wpcf7_ajax_loader', 'mm_wpcf7_ajax_loader');

/**
 * Login panel WP styles.
 */
function mm_login_styles() { ?>
<style type="text/css">
	.login h1 a {
		background-image: url('<?php echo THEME_URI; ?>/assets/images/logo.png');
		background-size: auto;
		display: block;
		width: 129px;
		height: 109px;
	}
</style>
<?php }
add_action('login_enqueue_scripts', 'mm_login_styles');

/**
 * Login panel WP change title.
 */
function mm_change_title_on_logo() {
	return get_bloginfo('title');
}
add_filter('login_headertitle', 'mm_change_title_on_logo');

/**
 * Login panel WP change url.
 */
function mm_loginpage_custom_link() {
	return get_bloginfo('url');
}
add_filter('login_headerurl', 'mm_loginpage_custom_link');