<?php
get_header();
?>
		<div class="page__title">
			<div class="container">
				<h1><?php _e('404', THEME_NAME); ?></h1>
			</div>
			<!-- .container -->
		</div>

		<section class="page-section section__single mt-none">
			<div class="container">
				<div class="row">
					<div class="gr-8 gr-12@xs">
						<div class="section__content">
							<p><?php _e('Wskazana podstrona nie istnieje. Wróć do ', THEME_NAME); ?><a href="/"><?php _e('strony głównej',THEME_NAME); ?></a>.</p>
						</div>
						<!-- .section__content -->
					</div>
					<!-- .gr -->
					<div class="gr-4 gr-12@xs">
						<div class="section__sidebar">
						</div>
						<!-- .section__sidebar -->
					</div>
					<!-- .gr -->
				</div>
				<!-- .row -->
			</div>
			<!-- .container -->
		</section>
		<!-- .section__single -->

<?php
get_footer(); 
?>