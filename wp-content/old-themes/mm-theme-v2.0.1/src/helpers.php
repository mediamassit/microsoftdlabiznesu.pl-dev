<?php

/**
 * Load a template part into with a list of arguments.
 * @param string $template
 * @param array $args
 * @param int $indent The number of tabs to prepend to each line of the output.
 * @return void
 */
function get_template_part_args($template, $args = array(), $indent = false) {
	extract((array) $args);

	if($indent > 0) {
		ob_start();
		include(locate_template($template . '.php'));
		$html = ob_get_clean();

		echo implode("\n".str_repeat('	', (int) $indent), explode("\n", $html));
	}
	else {
		include(locate_template($template . '.php'));
	}
}

/**
 * Check whether given variable is foreachable.
 * @param mixed $var
 * @return bool
 */
function is_foreachable($var = null) {
	return is_array($var) && sizeof($var) > 0;
}

if(!function_exists('get_paginate_links') && function_exists('paginate_links')) {
	/**
	 * Return paginate links array.
	 * @param mixed $args
	 * @return bool
	 */
	function get_paginate_links($args) {
		$links = paginate_links(array_merge($args, array('type' => 'array', 'prev_text' => 'PREV', 'next_text' => 'NEXT')));
		$data = array();

		if(is_array($links) && sizeof($links) > 0) {
			foreach($links as $link) {
				preg_match_all('#<([A-Za-z]+)\s?(.*?)>(.*?)<\/[A-Za-z]+>#', $link, $matches);
				
				$tag = @$matches[1][0];
				$atts = @$matches[2][0];
				$text = @$matches[3][0];

				if($tag == 'a') {
					preg_match('#href=(\'|")(.*?)(\'|")#', $atts, $link);
					$data[] = array(
						'text' => $text,
						'link' => @$link[2],
						'current' => false,
						'next' => $text === 'NEXT',
						'prev' => $text === 'PREV'
					);
				}

				if($tag == 'span' && (strpos($atts, 'dots') || strpos($atts, 'current'))) {
					$data[] = array(
						'text' => $text,
						'link' => null,
						'current' => strpos($atts, 'current') !== false,
						'next' => false,
						'prev' => false
					);
				}
			}
		}

		foreach($data as $i => $link) {
			if($link['link']) {
				$data[$i]['link'] .= ($_SERVER['QUERY_STRING'] && strpos($link['link'], '?') === false) ? '?'.$_SERVER['QUERY_STRING'] : '';
			}
		}

		return $data;
	}
}

add_filter( 'get_the_archive_title', function ($title) {
	if ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$title = single_tag_title( '', false );
	} elseif ( is_author() ) {
		$title = '<span class="vcard">' . get_the_author() . '</span>' ;
	}
	return $title;
});