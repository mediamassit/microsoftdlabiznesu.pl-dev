<?php
/**
 * Template Name: Strona główna
 */


get_header(); 

$args = array(
	'post_type' 		=> 'post',
	'orderby '			=> 'date',
	'order  '			=> 'ASC',
	'posts_per_page'	=> '8'
);
$news = new WP_Query($args);
$news =  $news->get_posts();
$news_parts_1 = array_slice($news, 0,4);
$news_parts_2 = array_slice($news, 4,3);
$slider = get_field('home_slider');
$promoted = get_field('promoted_post', option);
if (is_foreachable($promoted)) :
	$args2 = array(
	'post_type' 		=> 'post',
	'orderby '			=> 'date',
	'order  '			=> 'ASC',
	'posts_per_page'	=> '3',
	'post__in'			=> $promoted
	);
	$news2 = new WP_Query($args2);
	$news2 =  $news2->get_posts();
	$news_parts_2 = $news2;
endif;
?>
	<?php if(is_foreachable($slider)) : ?>
		<section class="section__slider">
				<div class="section__slider__wrapper">
					<?php foreach ($slider as $slide) : ?>
					<div class="section__slider__slide_js section__slider__slide">
						<img  src="<?php echo $slide['image']['url']; ?>" alt="" class="filter"/>
						<div class="section__slider__content">
							<h2 class="size-home-slider-title"><?php echo $slide['title']; ?></h2>
							<p class="size-home-slider-subtitle"><?php echo $slide['content']; ?></p>
							<?php if(!empty($slide['link'])) : ?><a href="<?php echo $slide['link']; ?>" class="more size-home-slider-more"><?php echo $slide['title_link']; ?></a><?php endif; ?>
						</div>
						<!-- .section__slider__content -->
					</div>
					<!-- .section__slider__slide -->
					<?php endforeach; ?>
				</div>
				<!-- .section__slider__wrapper -->
			
			<!-- .contaienr -->
		</section>
		<!-- .page-section -->
	<?php endif; ?>

		<div class="page-section section__home">
			<div class="container">
				<div class="row">
					<div class="gr-8  gr-12@xs">
						<div class="section__posts section__posts--tiles">
							<div class="row">
						<?php 
						foreach ($news_parts_1 as $i => $post) :
							setup_postdata($post);
							if($i % 2 == 0 && $i != 0) : 
							?>
							</div>
							<div class="row">
							<?php endif; ?>
								<div class="gr-6">
								<?php get_template_part( 'content','tile'); ?>
								</div>
						<?php endforeach; 
						wp_reset_postdata();
						?>
							</div>
							<!-- .row -->
						</div>
						<!-- .section__posts -->
						<?php /*
						<div class="section__cta">
						<?php 
						foreach ($news_parts_2 as $i => $post) :
							setup_postdata($post);
							get_template_part( 'content','cta');
						endforeach; 
						wp_reset_postdata();
						?>
						</div>
						<!-- .section__cta -->

						<div class="section__posts section__posts--lists">
						<?php 
						foreach ($news_parts_3 as $i => $post) :
							setup_postdata($post);
							get_template_part( 'content');
						endforeach; 
						wp_reset_postdata();
						?>
						<a href="<?php the_permalink(get_option('page_for_posts')); ?>" class="section__posts__see-all pull-right"><?php _e('Zobacz więcej >',THEME_NAME); ?></a>
						</div> */ ;?>
						<!-- .section__posts -->
						<a href="<?php the_permalink(get_option('page_for_posts')); ?>" class="section__posts__see-all pull-right"><?php _e('Zobacz więcej >',THEME_NAME); ?></a>
					</div>
					
					
					<!-- .gr -->
					<div class="gr-4 gr-12@xs">
						<div class="section__sidebar">
							<?php display_main_categories(); ?>
							<?php dynamic_sidebar('sidebar_default'); ?>
							<?php dynamic_sidebar('sidebar_home'); ?>
						</div>
						<!-- .section__sidebar -->
					</div>
					<!-- .gr -->
				</div>
				<!-- .row -->

				<section class="home-boxes-posts">
					<div class="row">
						<div class="section-padding">
							<?php 
							foreach ($news_parts_2 as $i => $post) :
							setup_postdata($post);
							if($i==0):
							?>
							<div class="gr-12 gr-12@xs no-padding">
								<a href="<?php echo get_permalink() ;?>">
									<div class="section_wraper">
										<?php echo the_post_thumbnail('post-box'); ?>
										<div class="section_post">
											<div class="v-middle">
												<div>
													<h2 class="color-white add-shadow"><?php echo $post->post_title; ?></h2>
													<p class="more add-shadow"><?php _e('Dowiedz się więcej...',THEME_NAME); ?></p>
												</div>
											</div>
										</div>
									</div>
								</a>
							</div>
							<?php
							else:
							?>
							<div class="gr-6 gr-12@xs no-padding">
								<a href="<?php echo get_permalink() ;?>">
									<div class="section_wraper">
										<?php echo the_post_thumbnail('post-box-small'); ?>
										<div class="section_post">
											<div class="v-middle">
												<div>
													<h2 class="color-white add-shadow"><?php echo $post->post_title; ?></h2>
													<p class="more add-shadow"><?php _e('Dowiedz się więcej...',THEME_NAME); ?></p>         
												</div>
											</div>
										</div>
									</div>
								</a>
							</div>
							<?php endif; ?>
							<?php endforeach; ?>
						</div>
					</div>
				</section>
			</div>
			<!-- .container -->
		</div>
<?php get_footer(); ?>