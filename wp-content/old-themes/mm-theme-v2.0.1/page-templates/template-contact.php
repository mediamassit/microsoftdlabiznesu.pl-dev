<?php
/**
 * Template Name: Kontakt
 */

get_header(); 

$forms = get_field('forms');
$content = get_field('content');

?>

<?php if ( have_posts() ) : ?>
		<section class="section">
			<div class="container">
				<div class="row">
					<div class="gr-7 gr-7@md gr-10@xs no-gutter-right no-gutter-right@md">
					<?php while(have_posts()) : the_post(); ?>
						<?php display_banner('page'); ?>
						<?php display_breadcrumb(); ?>
						<div class="gutter-right-30">
							<div class="content default-box">
								<h1><?php the_title(); ?></h1>
								<?php get_template_part_args('partials/top-bar'); ?>
								<div class="contact">
									<?php if(!get_the_content()) : ?>
									<p><?php _e('Brak treści na stronie. Wróć później.',THEME_NAME); ?></p>
										<?php else :
											the_content();
										endif; ?>
								</div>
								<?php 
									if(is_foreachable($forms)) {
										foreach ($forms as $i => $form) {
											$contact_form = $form['contact_form'];
											display_cf7($contact_form);
										}
									}
								?>
								<div id="map"></div>
								<?php if($content) : ?>
									<div class="mt-xxlg">
										<?php print $content; ?>
									</div>
								<?php endif; ?>
							</div>
							<!-- .content -->
						</div>
						<!-- .gutter -->
					</div>
					<!-- .gr -->
					<div class="gr-3 gr-3@md gr-10@xs no-gutter-left no-gutter-left@md gutter-left@xs">
						<div class="sidebar">
							<?php dynamic_sidebar( 'sidebar_default' ); ?>
						</div>
						<!-- .sidebar -->
					</div>
					<!-- .gr -->
				</div>
				<!-- .row -->
			</div>
			<!-- .container -->
		</section>

	<?php endwhile; ?>

<?php endif; ?>

<?php get_footer(); ?>