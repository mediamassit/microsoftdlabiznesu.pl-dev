<!doctype html>
<html lang="pl">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<script>var THEME_URL = '<?php echo get_template_directory_uri(); ?>'; var AJAXURL = '<?php echo admin_url('admin-ajax.php'); ?>'; var APP_LANGUAGE = 'pl'; (document.documentElement || document.getElementsByTagName('html')[0]).className += ' js';</script>
	<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/images/icon.png"/>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
	<!--[if lt IE 9]>
	<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script>
		var linkHome = '<?php echo home_lang_url(); ?>';
		var linkDir = '<?php echo THEME_URI; ?>/assets/images/';
	</script>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<div id="page">

		<nav id="menuMobile" style="display: none;">
			<ul>
				<?php
					wp_nav_menu(
						array(
							'container'			=> "",
							'container_class'	=> "",
							'container_id'		=> "",
							'fallback_cb'		=> false,
							'menu_class'		=> "",
							'before'			=> '',
							'after'				=> '',
							'link_before'		=> '',
							'link_after'		=> '',
							'items_wrap'		=> '%3$s',
							'theme_location'	=> "mobile",
							'walker'			=> '',
						)
					);
				?>
			</ul>
		</nav>
		<!-- #menuMobile -->

		<header class="site-header <?php if(is_archive() || is_single()) echo 'submenu-active'; ?>">
			<div class="site-header__top">
				<div class="container">
					<div class="site-header__top__inner">
						<div class="site-header__logo">
							<a href="<?php echo home_lang_url(); ?>">
								<img src="<?php echo THEME_URI; ?>/assets/images/logo.png" alt="" />
							</a>
						</div>
						<!-- .logo -->
						<nav class="site-header__nav-main hide@lg js-nav-main">
							<ul>
								<?php
									wp_nav_menu(
										array(
											'container'			=> "",
											'container_class'	=> "",
											'container_id'		=> "",
											'fallback_cb'		=> false,
											'menu_class'		=> "",
											'before'			=> '',
											'after'				=> '',
											'link_before'		=> '',
											'link_after'		=> '',
											'items_wrap'		=> '%3$s',
											'theme_location'	=> "primary",
											'walker'			=> '',
										)
									);
								?>
							</ul>
						</nav>
						<!-- .nav-main -->
						<div class="site-header__access">
								<form class="site-header__form-search" action="<?php echo get_home_url(); ?>" method="GET">
									<input class="site-header__form-search__input" type="text" placeholder="<?php echo get_field('search','option'); ?>" name="s" value="<?php echo $_GET['s']; ?>"/>
									<input class="site-header__form-search__submit" type="submit" value="Wyślij" />
								</form>
							<a href="#menuMobile" class="site-header__mobile"></a>
							<a href="<?php echo get_field('url_login','option'); ?>" class="site-header__access__login"><?php _e('Skontaktuj się z nami',THEME_NAME); ?></a>
						</div>
						<!-- .site-header__access -->
					</div>
					<!-- .site-header__top__inner -->
				</div>
				<!-- .container -->
			</div>
			<!-- .item-header__top -->
			<div class="site-header__bottom">

			</div>
			<!-- .site-header__bottom -->
		</header>
		<!-- .header -->