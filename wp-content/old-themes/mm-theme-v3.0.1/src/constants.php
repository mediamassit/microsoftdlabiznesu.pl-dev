<?php
/**
 * Constants.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

define( 'THEME_NAME', 'mm-theme' );

/* Sets the path to the parent theme directory. */
define( 'THEME_DIR', get_template_directory() );

/* Sets the path to the parent theme directory URI. */
define( 'THEME_URI', get_template_directory_uri() );

/* Sets the path to the core framework directory. */
define( 'DIR', trailingslashit( THEME_DIR ) . basename( dirname( __FILE__ ) ) );

define( 'SRC_DIR', trailingslashit( DIR ) );

define( 'EXT_DIR', trailingslashit( DIR ).'extensions' );