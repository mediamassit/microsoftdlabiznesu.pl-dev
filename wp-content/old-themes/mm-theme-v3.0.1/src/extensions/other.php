<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function display_cf7($form) {
	if( $form ) {
		echo do_shortcode('[contact-form-7 id="'.$form.'" html_class="form"]');
	} else {
		return false;
	}
}

function display_flags() {
	if ( !function_exists('icl_object_id') ) {
		return false;
	}
	$langs = icl_get_languages('skip_missing=0');
	if(sizeof((array) $langs) > 0) {
		$output = '<div class="flags"><ul>';
			foreach($langs as $lang) {
				$output .= '<li';
				$output .= ($lang['active'] == 1) ? ' class="active">' : '>';
				$output .= '<a href="'.$lang['url'].'"><img src="'.$lang['country_flag_url'].'" alt="" /></a></li>';
			}
		$output .= '</ul></div>';

		print $output;
	} else {
		return false;
	}
}

function home_lang_url() {
	if ( function_exists('icl_object_id') ) {
		return icl_get_home_url();
	} else {
		return home_url();
	}
}

function display_attachments($attachments) {
	if(!isset($attachments)) {
		return false;
	}

	if(is_foreachable($attachments)) {
		$output = '<hr>';
		$output .= '<p><strong>Załączniki</strong></p>';
		foreach ($attachments as $i => $attachment) {
			$file = $attachment['file'];
			$name = $attachment['name'];
			$output .= '<a href="'.$file['url'].'" class="attachment" download>'.$name.'</a>';
		}
	} else {
		return false;
	}
	print $output;
}

?>