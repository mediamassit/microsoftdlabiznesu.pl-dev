;(function($) {
	
	

	$('.current-menu-ancestor').addClass('active');
	if($('.js-nav-main li').hasClass('current-menu-ancestor')) {
		$('.site-header').addClass('submenu-active');
	}
	
	function ResizeSlider() {
	var h = window.outerHeight;
	var w = window.innerWidth;
		var page = window.location.pathname;
		if( w > 992) {
			if(page == '/' || page == '/default.aspx'){
				$('.section__slider__slide_js').css("height",h-153);
			}
			else{
				$('.section__slider__slide_js').css("height",h-219);
			}
		}
//		else {
//			$('.section__slider__slide_js').css("height",400);
//		}
	};
	ResizeSlider();
	
	$( window ).resize(function() {
  		ResizeSlider();
	});
	setInterval(function(){ 
		$( ".section__slider .section__slider__slide_js .filter" ).toggleClass( 'animate' );
	}, 10000);
	setTimeout(function(){
		$(".section__slider .section__slider__slide_js .filter").addClass("animate");
	},100);

})(jQuery);