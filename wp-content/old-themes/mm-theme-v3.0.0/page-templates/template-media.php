<?php 
/*
Template Name: Media o nas
*/
get_header();

$forms = get_field('forms');

?>
		<section class="section">
			<div class="container">
				<div class="row">
					<div class="gr-7 gr-7@md gr-10@xs no-gutter-right no-gutter-right@md">
						<?php display_banner('page'); ?>
						<?php display_breadcrumb(); ?>
						<div class="gutter-right-30">
							<div class="content default-box">
								<?php get_template_part_args('partials/media'); ?>
								<?php 
									if(is_foreachable($forms)) {
										foreach ($forms as $i => $form) {
											$contact_form = $form['contact_form'];
											display_cf7($contact_form);
										}
									}
								?>
							</div>
							<!-- .content -->
						</div>
						<!-- .gutter -->
					</div>
					<!-- .gr -->
					<div class="gr-3 gr-3@md gr-10@xs no-gutter-left no-gutter-left@md gutter-left@xs">
						<div class="sidebar">
							<?php dynamic_sidebar( 'sidebar_default' ); ?>
						</div>
						<!-- .sidebar -->
					</div>
					<!-- .gr -->
				</div>
				<!-- .row -->
			</div>
			<!-- .container -->
		</section>

<?php get_footer(); ?>


	<div class="section light-grey-bg">
		<div class="container">
			
				
			</div>
			
		</div>
	</div>


