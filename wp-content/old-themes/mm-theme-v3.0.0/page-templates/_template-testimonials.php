<?php

global $wp_query;
get_header();

$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$posts_per_page = 10;
$args = array(
	'post_type' => 'testimonial',
	'posts_per_page' => $posts_per_page,
	'paged' => $paged
);

$forms = get_field('forms');
$testimonial = new WP_Query($args);
?>

		<section class="section">
			<div class="container">
				<div class="row">
					<div class="gr-7 gr-7@md gr-10@xs no-gutter-right no-gutter-right@md">
						<?php display_banner('page'); ?>
						<?php display_breadcrumb(); ?>
						<div class="gutter-right-30">
							<div class="content default-box">
								<h1><?php the_title(); ?></h1>
								<?php get_template_part_args('partials/top-bar'); ?>
								<?php if($testimonial->have_posts()) : ?>
								<?php
									$content = get_the_content();
									$content = apply_filters('the_content', $content);
								?>
								<?php if(!$content) : ?>
									<p><?php _e('Brak treści na stronie. Wróć później.',THEME_NAME); ?></p>
								<?php else :
										echo $content;
								endif;
								?>
								<div class="testiomonials">
									<?php while ( $testimonial->have_posts() ) : 
										$testimonial->the_post(); ?>
										<?php
											$featured_image = @wp_get_attachment_image_src(@get_post_thumbnail_id(), 'full');
										?>
									<div class="item">
										<h4><?php the_title(); ?></h4>

										<a href="<?php echo $featured_image[0]; ?>"><img src="<?php echo $featured_image[0]; ?>" alt="" /></a>
									</div>
									<?php endwhile; ?>
									<?php wp_reset_postdata(); ?>
								</div>
								<?php
									$pagination = get_paginate_links(array(
										'format' => 'page/%#%',
										'current' => max(1, get_query_var('paged')),
										'total' => $testimonial->max_num_pages,
									));

									echo get_template_part_args('partials/pagination', array('pagination' => $pagination, 'max_pages' => $testimonial->max_num_pages));
								?>
								<?php else: ?>
									<p><?php _e('Brak opinii.',THEME_NAME); ?></p>
								<?php endif; ?>
								<?php 
									if(is_foreachable($forms)) {
										foreach ($forms as $i => $form) {
											$contact_form = $form['contact_form'];
											display_cf7($contact_form);
										}
									}
								?>
							</div>
							<!-- .content -->
						</div>
						<!-- .gutter -->
					</div>
					<!-- .gr -->
					<div class="gr-3 gr-3@md gr-10@xs no-gutter-left no-gutter-left@md gutter-left@xs">
						<div class="sidebar">
							<?php dynamic_sidebar( 'sidebar_default' ); ?>
						</div>
						<!-- .sidebar -->
					</div>
					<!-- .gr -->
				</div>
				<!-- .row -->
			</div>
			<!-- .container -->
		</section>

<?php get_footer(); ?>