<?php

// $wpseo_primary_term = new WPSEO_Primary_Term( 'category', $id );
// $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
// $primary_cat = get_term( $wpseo_primary_term );

?>

<article class="section__posts__post rwd-padding">
	<div class="row">
		<div class="gr-4">
			<a href="<?php the_permalink(); ?>">
			<?php if(has_post_thumbnail()) : ?>
				<?php the_post_thumbnail('thumb-235x160'); ?>
			<?php endif; ?>
			</a>
		</div>
		<!-- .gr -->
		<div class="gr-8">
			<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
			<?php if($post->post_excerpt): ?>
			<?php the_excerpt(); ?>
			<?php else: ?>
			<p><?php echo wp_trim_words(get_the_content(), 15, '...'); ?></p>
			<?php endif; ?>
			<a href="<?php the_permalink(); ?>" class="more"><?php _e('Dowiedz się więcej',THEME_NAME); ?></a>
		</div>
		<!-- .gr -->
	</div>
	<!-- .row -->
</article>
