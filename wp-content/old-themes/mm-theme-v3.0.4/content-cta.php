<?php

// $wpseo_primary_term = new WPSEO_Primary_Term( 'category', $id );
// $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
// $primary_cat = get_term( $wpseo_primary_term );
?>

<a href="<?php the_permalink(); ?>">
	<?php if(has_post_thumbnail()) : ?>
		<?php the_post_thumbnail('thumb-770x400'); ?>
	<?php endif; ?>
	<div class="section__cta__content">
		<h3 class="color-white"><?php the_title(); ?></h3>
		<?php if($post->post_excerpt): ?>
		<?php the_excerpt(); ?>
		<?php else: ?>
		<p class="color-white"><?php echo wp_trim_words(get_the_content(), 15, '...'); ?></p>
		<?php endif; ?>
		<span class="more"><?php _e('Dowiedz się więcej',THEME_NAME); ?></span>
	</div>
	<!-- .section__cta__content -->
</a>