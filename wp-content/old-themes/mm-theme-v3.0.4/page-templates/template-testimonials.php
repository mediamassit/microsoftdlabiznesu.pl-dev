<?php

/**
 * Template Name: Opinie
 */
global $wp_query;
get_header();

$forms = get_field('forms');
?>

		<section class="section">
			<div class="container">
				<div class="row">
					<div class="gr-7 gr-7@md gr-10@xs no-gutter-right no-gutter-right@md">
						<?php display_banner('page'); ?>
						<?php display_breadcrumb(); ?>
						<div class="gutter-right-30">
							<div class="content default-box">
								<?php while(have_posts()) : the_post(); ?>
								<h1><?php the_title(); ?></h1>
								<?php
								get_template_part_args('partials/top-bar'); 
								$content = get_the_content();
								$content = apply_filters('the_content', $content);
								?>
								<?php if(!$content) : ?>
									<p><?php _e('Brak opinii. Wróć później.',THEME_NAME); ?></p>
								<?php else :
										echo $content;
								endif;
								if(is_foreachable($forms)) {
									foreach ($forms as $i => $form) {
										$contact_form = $form['contact_form'];
										display_cf7($contact_form);
									}
								}
								?>
								<?php endwhile; ?>
							</div>
							<!-- .content -->
						</div>
						<!-- .gutter -->
					</div>
					<!-- .gr -->
					<div class="gr-3 gr-3@md gr-10@xs no-gutter-left no-gutter-left@md gutter-left@xs">
						<div class="sidebar">
							<?php dynamic_sidebar( 'sidebar_default' ); ?>
						</div>
						<!-- .sidebar -->
					</div>
					<!-- .gr -->
				</div>
				<!-- .row -->
			</div>
			<!-- .container -->
		</section>

<?php get_footer(); ?>