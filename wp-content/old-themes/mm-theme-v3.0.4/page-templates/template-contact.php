<?php
/**
 * Template Name: Kontakt
 */

get_header(); 

while(have_posts()) : the_post();
?>
		<div class="page__title">
			<div class="container">
				<h1><?php the_title(); ?></h1>
			</div>
			<!-- .container -->
		</div>

		<div class="page-section section__single mt-none">
			<div class="container">
				<div class="row">
					<div class="gr-8 gr-12@xs">
						<div class="section__single__thumbnail<?php if(!has_post_thumbnail()) echo ' mb-none'; ?>">
						<?php if(has_post_thumbnail()) : ?>
							<?php the_post_thumbnail('full'); ?>
						<?php endif; ?>
						</div>
						<div class="section__content">
						<?php
							$content = get_the_content();
							$content = apply_filters('the_content', $content);
							if(!$content) {
						?>
								<p><?php _e(' ',THEME_NAME); ?></p>
						<?php 
							} else {
								echo $content;
							}
						?>
						</div>
						<!-- .section__content -->
					</div>
					<!-- .gr -->
					<div class="gr-4 gr-12@xs">
						<div class="section__sidebar">
							<?php display_categories(get_the_id(),true); ?>
							<?php dynamic_sidebar('sidebar_contact'); ?>
						</div>
						<!-- .section__sidebar -->
					</div>
					<!-- .gr -->
				</div>
				<!-- .row -->
			</div>
			<!-- .container -->
		</div>
		<!-- .section__single -->

		<?php
		$args = array(
			'post_type' 		=> 'post',
			'orderby '			=> 'date',
			'order  '			=> 'ASC',
			'posts_per_page'	=> '6'
		);
		$news = new WP_Query($args);
		$i = 0;
		if ( $news->have_posts() ) :
		?>
		<section class="section__posts section__posts--last">
			<div class="container">
				<div class="section__posts__wrapper">
					<h3><?php _e('Ostatnio dodane',THEME_NAME); ?></h3>
					<div class="row">
					<?php while($news->have_posts()) : $news->the_post(); ?>
						<?php if($i == 3) echo '</div><div class="row">'; ?>
						<div class="gr-4 gr-6@sm">
						<?php get_template_part( 'content','tile' ); ?>
						</div>
						<?php $i++; ?>
					<?php endwhile; ?>
					</div>
				</div>
				<!-- .section__posts__wrapper -->
			</div>
			<!-- .container -->
		</section>
		<!-- .section__posts section__posts -->
		<?php endif; ?>

<?php
endwhile;

get_footer(); 
?>