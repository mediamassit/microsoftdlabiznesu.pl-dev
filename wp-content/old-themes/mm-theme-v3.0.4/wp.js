;(function($) {
	
	

	$('.current-menu-ancestor').addClass('active');
	if($('.js-nav-main li').hasClass('current-menu-ancestor')) {
		$('.site-header').addClass('submenu-active');
	}
	
	function ResizeSlider() {
	var h = $( window ).height();
	var w = $( window ).width();
		var page = window.location.pathname;
		if( w > 992) {
			if(page == '/' || page == '/default.aspx'){
				$('.section__slider__slide_js').css("height",h-80);
			}
			else{
				$('.section__slider__slide_js').css("height",h-140);
			}
		}
//		else {
//			$('.section__slider__slide_js').css("height",400);
//		}
	};
	ResizeSlider();
	
	$( window ).resize(function() {
  		ResizeSlider();
	});
	setInterval(function(){ 
		$( ".section__slider .section__slider__slide .filter" ).toggleClass( 'animate' );
	}, 10000);
	setTimeout(function(){
		$(".section__slider .section__slider__slide .filter").addClass("animate");
	},100);
	
	var interval;
	var animate = function(){
		interval = setInterval(function(){ 
			$( ".section__tabs__single.active .animate_js" ).toggleClass( 'transform' );
		}, 10000);
		setTimeout(function(){
			$(".section__tabs__single.active .animate_js").addClass("transform");
		},100);
		$( ".section__tabs__single .animate_js" ).removeClass( 'transform' );
	}
	animate();
	$('.section__tabs__nav ul li a').click(function(){
	   clearInterval(interval);
	   animate();
	});
})(jQuery);