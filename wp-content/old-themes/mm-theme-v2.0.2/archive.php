<?php
get_header();
$cat_current = get_query_var('cat');
$title = get_field('archive_title','category_'.$cat_current);
$description = get_field('archive_short_desc','category_'.$cat_current);
$image = get_field('archive_image','category_'.$cat_current);
$archive_tabs = get_field('archive_tabs','options');
$slider = get_field('slider_display','category_'.$cat_current);
if(get_field('archive_tabs','category_'.$cat_current)) {
	$archive_tabs = get_field('archive_tabs','category_'.$cat_current);
}
$promoted = get_field('promoted_post', 'category_'.$cat_current);

?>
		<?php if(!empty($slider)) { ;?>
		<section class="section__slider">
<!--			<div class="container">-->
				<div class="section__slider__wrapper">
					<div class="section__slider__slide_js section__slider__slide  bg--before">
						<div class="before"></div>
					<?php if($image) : ?>
						<img class="filter" src="<?php echo $image['sizes']['full_hd']; ?>" alt="<?php if($image["alt"]) { echo $image["alt"]; } else { echo $image["title"]; }  ?>" />
					<?php endif; ?>
						<div class="container">
							<div class="section__slider__content">
								<div class="v-middle">
									<div>
										<h1 class="custom-h1-slider"><?php if($title) { echo $title; } else { the_archive_title(); } ?></h1>
										<p class="custom-p-slider"><?php echo $description; ?></p>
									</div>
								</div>
							</div>
						</div>
						<!-- .section__slider__content -->
					</div>
					<!-- .section__slider__slide -->
				</div>
				<!-- .section__slider__wrapper -->
<!--			</div>-->
			<!-- .contaienr -->
		</section>
		<!-- .page-section -->
		<?php } else { ;?>
				<section class="section__slider">
			<div class="container">
				<div class="section__slider__wrapper">
					<div class="section__slider__slide bg--before">
						<div class="before"></div>
					<?php if($image) : ?>
						<img src="<?php echo $image['sizes']['intro-1170x440']; ?>" alt="<?php if($image["alt"]) { echo $image["alt"]; } else { echo $image["title"]; } ?>" />
					<?php endif; ?>
						<div class="section__slider__content">
							<div class="v-middle">
								<div>
									<h1><?php if($title) { echo $title; } else { the_archive_title(); } ?></h1>
									<p><?php echo $description; ?></p>
								</div>
							</div>
						</div>
						<!-- .section__slider__content -->
					</div>
					<!-- .section__slider__slide -->
				</div>
				<!-- .section__slider__wrapper -->
			</div>
			<!-- .contaienr -->
		</section>
		<!-- .page-section -->
		<?php } ;?>

		<div class="page-section section__home">
			<div class="container">
				<div class="row">
					<div class="gr-8  gr-12@xs">
						<div class="section__posts section__posts--lists">
							<?php
							if ( have_posts() ) :
								while(have_posts()) : the_post();
							?>
								<?php get_template_part( 'content' ); ?>
							<?php
								endwhile;
							else:
							?>
								<h4><?php _e('Brak wpisów w wybranej kategorii.',THEME_NAME); ?></h4>
							<?php endif; ?>
							<?php display_pagination(get_query_var('paged'),$wp_query->max_num_pages); ?>
						</div>
						<!-- .section__posts -->
					</div>
					<!-- .gr -->
					<div class="gr-4 gr-12@xs">
						<div class="section__sidebar">
							<?php display_categories(get_the_id()); ?>
							<?php dynamic_sidebar('sidebar_default'); ?>
							<?php dynamic_sidebar('sidebar_single'); ?>
						</div>
						<!-- .section__sidebar -->
					</div>
					<!-- .gr -->
				</div>
				<!-- .row -->
			</div>
			<!-- .container -->
		</div>
<!--		-->
	<?php	if (is_foreachable($promoted)) :
	$args2 = array(
	'post_type' 		=> 'post',
	'orderby '			=> 'date',
	'order  '			=> 'ASC',
	'posts_per_page'	=> '4',
	'post__in'			=> $promoted
	);
	$news2 = new WP_Query($args2);
	$news2 =  $news2->get_posts();
	$news_parts_2 = $news2;
endif;
?>
			<?php	if (is_foreachable($promoted)) : ;?>
			<section class="home-boxes-posts">
					<div class="container">
						<div class="row">
							<div class="section-padding">
								<?php 
								foreach ($news_parts_2 as $i => $post) :
								setup_postdata($post);
								
								?>
								<div class="gr-6 gr-12@xs no-padding">
									<a href="<?php echo get_permalink() ;?>">
										<div class="section_wraper">
											<?php echo the_post_thumbnail('post-box-small'); ?>
											<div class="section_post">
												<div class="v-middle">
													<div>
														<h2 class="color-white add-shadow"><?php echo $post->post_title; ?></h2>
														<p class="more add-shadow"><?php _e('Dowiedz się więcej...',THEME_NAME); ?></p>         
													</div>
												</div>
											</div>
										</div>
									</a>
								</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
			</section>
			<?php endif ; ?>
		<?php if($archive_tabs) : ?>
		<div class="section__tabs">
			<div class="container">
				<div class="section__tabs__wrapper">
					<div class="row">
						<div class="gr-3 gr-12@xs">
							<nav class="section__tabs__nav js-nav-tabs">
								<ul>
								<?php foreach($archive_tabs as $i => $tab) : ?>
									<li><a href="#tab_<?php echo $i; ?>" <?php if($i == 0) echo 'class="active"'; ?>><?php echo $tab['title']; ?></a></li>
								<?php endforeach; ?>
								</ul>
							</nav>
						</div>
						<!-- .gr -->
						<div class="gr-9 gr-12@xs">
							<div class="section__tabs__tab">
								<?php foreach($archive_tabs as $i => $tab) : ?>
								<div class="section__tabs__single <?php if($i == 0) echo 'active'; ?>" id="tab_<?php echo $i; ?>" <?php if($tab): ?>style="background-image: url('<?php echo $tab['image']['sizes']['thumb-900x500']; ?>');"<?php endif; ?>>
									<?php if($tab['content']) : ?>
									<div class="section__tabs__content bg--yellow">
										<?php echo $tab['content']; ?>
									</div>
									<!-- .section__tabs__content -->
									<?php endif; ?>
								</div>
								<!-- .section__tabs__single -->
								<?php endforeach; ?>
							</div>
							<!-- section__tabs__tab -->
						</div>
						<!-- .gr -->
					</div>
					<!-- .row -->
				</div>
				<!-- .section__tabs__wrapper -->
			</div>
			<!-- .contaienr -->
		</div>
		<!-- .section__tabs -->
		<?php endif; ?>
<?php

get_footer(); 
?>