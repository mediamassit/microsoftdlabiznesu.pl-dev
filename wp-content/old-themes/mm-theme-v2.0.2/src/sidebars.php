<?php

add_action( 'widgets_init', 'dmxp_widgets' );
function dmxp_widgets() {
	register_sidebar( array(
		'name' => __( 'Domyślny sidebar', 'dmx-plugin' ),
		'id' => 'sidebar_default',
		'description' => __( '', 'dmx-plugin' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
}