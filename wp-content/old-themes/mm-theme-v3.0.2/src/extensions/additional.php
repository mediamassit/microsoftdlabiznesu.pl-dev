<?php

function display_categories($id, $single = false) {
	if($single === true) {
		$wpseo_primary_term = new WPSEO_Primary_Term( 'category', $id );
		$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
		$primary_cat = get_term( $wpseo_primary_term );
		if(!$primary_cat->errors > 0) {
			$args = array('parent' => $primary_cat->term_id,'orderby' => 'menu_order');
			$categories = get_categories( $args );
		} else {
			$categories = '';
		}
	} else {
		$cat_current = get_query_var('cat');

		$category = get_category($cat_current);
		if($category->parent && $category->parent != 0) {
			$args = array('parent' => $category->parent, 'orderby' => 'menu_order');
		} else {
			$args = array('parent' => $cat_current,'orderby' => 'menu_order');
		}
		$categories = get_categories( $args );
	}
	if(is_foreachable($categories) && !(sizeof($categories) == 1 && $categories[0]->term_id == $cat_current)) {
		$output .= '<div class="widget widget--menu">';
		$output .= '<h4>'.__('Kategorie',THEME_NAME).'</h4>';
		$output .= '<ul>';

		foreach($categories as $cat) {
			$cat_url = get_term_link($cat);
			if($cat_current != $cat->term_id) {
				$output .= '<li><a href="'.$cat_url.'">';
				$output .= $cat->name;
				$output .= ' <span class="number">('.$cat->count.')</span>';
				$output .= '</a></li>';
			}
		}
		$output .= '</ul>';
		$output .= '</div>';
	}
	print $output;
}

function display_main_categories() {
	$args = array('parent' => 0,'orderby' => 'menu_order');
	$categories = get_categories( $args );
	if(is_foreachable($categories)) {
		$output .= '<div class="widget widget--menu">';
		$output .= '<h4>'.__('Kategorie',THEME_NAME).'</h4>';
		$output .= '<ul>';

		foreach($categories as $cat) {
			$cat_url = get_term_link($cat);
			if($cat_current != $cat->term_id) {
				$output .= '<li><a href="'.$cat_url.'">';
				$output .= $cat->name;
				$output .= ' <span class="number">('.$cat->count.')</span>';
				$output .= '</a></li>';
			}
		}
		$output .= '</ul>';
		$output .= '</div>';
	}
	print $output;
}

function display_pagination($paged, $total) {
	$pagination = get_paginate_links(array(
		'format' => 'page/%#%',
		'current' => max(1, $paged),
		'total' => $total,
	));
	if($total > 1) {
		foreach($pagination as $link) {
			if($link['next']) {
				$output .= '<a href="'.$link['link'].'" class="section__posts__see-all pull-right">'.__('Następne',THEME_NAME).' ></a>';
			} else if($link['prev']) {
				$output .= '<a href="'.$link['link'].'" class="section__posts__see-all pull-left">< '.__('Poprzednie',THEME_NAME).'</a>';
			}
		}
	}
	print $output;
}