<?php

// $wpseo_primary_term = new WPSEO_Primary_Term( 'category', $id );
// $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
// $primary_cat = get_term( $wpseo_primary_term );
$crafts = wp_get_post_terms(get_the_id(), 'post_craft');
$advantages = wp_get_post_terms(get_the_id(), 'post_advantage');
?>

<article class="section__posts__post">
	<figure>
		<a href="<?php the_permalink(); ?>">
		<?php if(has_post_thumbnail()) : ?>
			<?php the_post_thumbnail('thumb-370x200'); ?>
		<?php endif; ?>
		</a>
		<figcaption>
			<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
			<?php if($post->post_excerpt): ?>
			<p><?php echo wp_trim_words(get_the_excerpt(), 10, '...'); ?></p>
			<?php else: ?>
			<p><?php echo wp_trim_words(get_the_content(), 10, '...'); ?></p>
			<?php endif; ?>
			<?php if(is_foreachable($crafts)) : ?>
				<p class="section__posts__post__tax section__posts__post__tax--first" style="height: auto;"><strong><?php _e('Branża',THEME_NAME); ?>: </strong>
				<?php foreach ($crafts as $i => $craft) : ?>
					<a href="<?php echo get_term_link($craft->term_id); ?>"><?php echo $craft->name; ?></a><?php if(!$i+1 == sizeof($crafts)) echo ', '; ?>
				<?php endforeach; ?>
				</p>
			<?php endif; ?>
			<?php if(is_foreachable($advantages)) : ?>
				<p class="section__posts__post__tax" style="height: auto;"><strong><?php _e('Rezultaty',THEME_NAME); ?>: </strong>
				<?php foreach ($advantages as $j => $advantage) : ?>
					<a href="<?php echo get_term_link($advantage->term_id); ?>"><?php echo $advantage->name; ?></a><?php if(!$j+1 == sizeof($advantages)) echo ', '; ?>
				<?php endforeach; ?>
				</p>
			<?php endif; ?>
			<a href="<?php the_permalink(); ?>" class="more"><?php _e('Dowiedz się więcej',THEME_NAME); ?></a>
		</figcaption>
	</figure>
</article>