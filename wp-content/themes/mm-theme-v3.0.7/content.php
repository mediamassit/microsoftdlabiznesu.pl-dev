<?php

// $wpseo_primary_term = new WPSEO_Primary_Term( 'category', $id );
// $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
// $primary_cat = get_term( $wpseo_primary_term );
$crafts = wp_get_post_terms(get_the_id(), 'post_craft');
$advantages = wp_get_post_terms(get_the_id(), 'post_advantage');
?>
<article class="section__posts__post">
	<div class="row">
		<div class="gr-4">
			<a href="<?php the_permalink(); ?>">
			<?php if(has_post_thumbnail()) : ?>
				<?php the_post_thumbnail('thumb-235x160'); ?>
			<?php endif; ?>
			</a>
		</div>
		<!-- .gr -->
		<div class="gr-8">
			<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
			<?php if($post->post_excerpt): ?>
			<?php the_excerpt(); ?>
			<?php else: ?>
			<p><?php echo wp_trim_words(get_the_content(), 15, '...'); ?></p>
			<?php endif; ?>
			<?php if(is_foreachable($crafts)) : ?>
				<p class="mb-none mt-sm" style="height: auto;"><strong><?php _e('Branża',THEME_NAME); ?>: </strong>
				<?php foreach ($crafts as $i => $craft) : ?>
					<a href="<?php echo get_term_link($craft->term_id); ?>"><?php echo $craft->name; ?></a><?php if(!$i+1 == sizeof($crafts)) echo ', '; ?>
				<?php endforeach; ?>
				</p>
			<?php endif; ?>
			<?php if(is_foreachable($advantages)) : ?>
				<p class="mb-none mt-none" style="height: auto;"><strong><?php _e('Rezultaty',THEME_NAME); ?>: </strong>
				<?php foreach ($advantages as $i => $advantage) : ?>
					<a href="<?php echo get_term_link($advantage->term_id); ?>"><?php echo $advantage->name; ?></a><?php if(!$i+1 == sizeof($crafts)) echo ', '; ?>
				<?php endforeach; ?>
				</p>
			<?php endif; ?>
			<a href="<?php the_permalink(); ?>" class="more"><?php _e('Dowiedz się więcej',THEME_NAME); ?></a>
		</div>
		<!-- .gr -->
	</div>
	<!-- .row -->
</article>