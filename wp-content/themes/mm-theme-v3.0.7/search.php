<?php get_header();
$slider = get_field('home_slider');
?>
	<?php if(is_foreachable($slider)) : ?>
		<section class="section__slider">
			<div class="container">
				<div class="section__slider__wrapper">
					<?php foreach ($slider as $slide) : ?>
					<div class="section__slider__slide">
						<img src="<?php echo $slide['image']['url']; ?>" alt="" />
						<div class="section__slider__content bg--yellow">
							<h2><?php echo $slide['title']; ?></h2>
							<p><?php echo $slide['content']; ?></p>
						</div>
						<!-- .section__slider__content -->
					</div>
					<!-- .section__slider__slide -->
					<?php endforeach; ?>
				</div>
				<!-- .section__slider__wrapper -->
			</div>
			<!-- .contaienr -->
		</section>
		<!-- .page-section -->
	<?php endif; ?>
		<!-- .section-realization -->

		<section class="page-section section__home">
			<div class="container">
				<div class="row">
					<div class="gr-8  gr-12@xs">
						<div class="section__posts section__posts--lists">
							<div class="row">
								<?php if (have_posts()) : ?>
									<h3><?php printf( __( 'Wyniki wyszukiwania dla: %s', THEME_NAME), get_search_query() ); ?></h3>
									<h4><?php _e('Znaleziono:',THEME_NAME); ?> <?php echo $wp_query->found_posts; ?></h4>
									<hr>
									<?php while (have_posts()) : the_post() ; ?>
											<?php get_template_part( 'content'); ?>
									<?php endwhile; ?>
									
								<?php else: ?>
									<h3><?php _e('Niczego nie znaleziono.',THEME_NAME); ?></h3>
									<hr>
									<h4> <?php _e('Spróbuj ponownie:',THEME_NAME); ?></h4>
									<form class="search" action="<?php echo home_lang_url(); ?>/" style="display: inline-block;">
										<input type="text" name="s" placeholder="SZUKAJ">
										<input type="submit" value="Szukaj">
									</form>
								<?php endif; ?>
							</div>
							<div class="row">
								<div class="gr-24">
									<nav >
									<?php
									$pagination = get_paginate_links(array(
										'format' => 'page/%#%',
										'current' => max(1, get_query_var('paged')),
										'total' => $wp_query->max_num_pages,
									));
									echo get_template_part_args('partials/pagination', array('pagination' => $pagination, 'max_pages' => $wp_query->max_num_pages));
								?>
									</nav>
								</div>
								<!-- .gr -->
							</div>
						</div>
						<!-- .page-section__inner -->
					</div>
					<!-- .gr -->
					<div class="gr-4 gr-12@xs">
						<div class="section__sidebar">
							<?php display_main_categories(); ?>
							<?php dynamic_sidebar('sidebar_default'); ?>
							<?php dynamic_sidebar('sidebar_single'); ?>
						</div>
						<!-- .section__sidebar -->
					</div>
				<!-- .row -->
			</div>
			<!-- .container -->
		</section>
<?php //endwhile; wp_reset_query(); ?>
<?php get_footer(); ?>