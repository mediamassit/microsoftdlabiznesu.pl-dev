;(function($) {
	
	

	$('.current-menu-ancestor').addClass('active');
	if($('.js-nav-main li').hasClass('current-menu-ancestor')) {
		$('.site-header').addClass('submenu-active');
	}

	function ResizeSlider() {
	var h = $( window ).height();
	var w = $( window ).width();
		var page = window.location.pathname;
		if( w > 992) {
			if(page == '/' || page == '/default.aspx'){
				$('.section__slider__slide_js').css("height",h-80);
			}
			else{
				$('.section__slider__slide_js').css("height",h-140);
			}
		}
//		else {
//			$('.section__slider__slide_js').css("height",400);
//		}
	};
	ResizeSlider();
	
	$( window ).resize(function() {
			ResizeSlider();
	});
	setInterval(function(){ 
		$( ".section__slider .section__slider__slide .filter" ).toggleClass( 'animate' );
	}, 10000);
	setTimeout(function(){
		$(".section__slider .section__slider__slide .filter").addClass("animate");
	},100);
	
	var interval;
	var animate = function(){
		interval = setInterval(function(){ 
			$( ".section__tabs__single.active .animate_js" ).toggleClass( 'transform' );
		}, 10000);
		setTimeout(function(){
			$(".section__tabs__single.active .animate_js").addClass("transform");
		},100);
		$( ".section__tabs__single .animate_js" ).removeClass( 'transform' );
	}
	animate();
	$('.section__tabs__nav ul li a').click(function(){
		 clearInterval(interval);
		 animate();
	});

	$('.section__posts__post, .section_post, .section__content, .widget').each(function() {
		var $html = $(this).html();
		var $text = $html.replace(/(\s)([\S])[\s]+/g,"$1$2&nbsp;");
		$(this).html($text);
	});

	// $('.js-nav-main .stable-width .sub-menu').each(function() {
	// 	var qty = $(this).find('li').size();
	// 	console.log(qty);
	// 	var widthPercent = 100/qty;
	// 	$(this).find('li').css('width', widthPercent+'%');
	// });

	$('.js-carousel-home').owlCarousel({
		loop: false,
		nav: true,
		margin:0,
		responsiveClass:true,
		responsive:{
				0:{
						items:1,
						nav: false
				},
				600:{
						items:2
				},
				1200:{
						items:3
				}
		}
	});

	$('.owl-carousel').owlCarousel({
			video:true,
			loop:true,
			autoplay:true,
			autoplayTimeout:15000,
			smartSpeed:1000,
			margin:0,
			nav:false,
			dots:true,
			items:1
	});
	$('.right-arrow').click(function() {
		$('.owl-carousel').trigger('next.owl.carousel');
	});
	$('.left-arrow').click(function() {
		$('.owl-carousel').trigger('prev.owl.carousel');
	});
})(jQuery);
