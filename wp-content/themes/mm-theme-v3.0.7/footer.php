<?php

$facebook = get_field('link_facebook','option');
$twitter = get_field('link_twitter','option');
$youtube = get_field('link_youtube','option');
$linkedin = get_field('link_linkedin','option');
$footer_text = get_field('footer_text','option');
$yellow_box = get_field('yellow_box','option');
?>

		<?php if($yellow_box === true) : ?>
		<section class="page-section section__newsletter">
			<div class="container">
				<div class="section__newsletter__wrapper bg--yellow">
					<div class="row">
						<div class="gr-6 gr-12@xs">
							<div class="section__newsletter__menu">
								<ul>
									<?php
										wp_nav_menu(
											array(
												'container'			=> "",
												'container_class'	=> "",
												'container_id'		=> "",
												'fallback_cb'		=> false,
												'menu_class'		=> "",
												'before'			=> '',
												'after'				=> '',
												'link_before'		=> '',
												'link_after'		=> '',
												'items_wrap'		=> '%3$s',
												'theme_location'	=> "footer_orange_box",
												'walker'			=> '',
											)
										);
									?>
								</ul>
							</div>
							<!-- .section__newsletter__menu -->
						</div>
						<!-- .gr -->
						<div class="gr-6 gr-12@xs">
							<?php echo $footer_text; ?>
						</div>
						<!-- .gr -->
					</div>
					<!-- .row -->
				</div>
				<!-- .section__newsletter__wrapper -->
			</div>
			<!-- .container -->
		</section>
		<!-- .section__newsletter -->
		<?php endif; ?>

		<footer class="site-footer">
			<div class="site-footer__top">
				<div class="container">
					<h3><?php _e('Obserwuj Microsoft dla Biznesu',THEME_NAME); ?></h3>
					<nav class="nav--socials">
						<ul>
							<?php if($facebook) : ?>
							<li><a href="<?php echo $facebook; ?>" target="_blank"><i class="icon icon-fcb"></i></a></li>
							<?php endif; ?>
							<?php if($twitter) : ?>
							<li><a href="<?php echo $twitter; ?>" target="_blank"><i class="icon icon-insta"></i></a></li>
							<?php endif; ?>
							<?php if($youtube) : ?>
							<li><a href="<?php echo $youtube; ?>" target="_blank"><i class="icon icon-yt"></i></a></li>
							<?php endif; ?>
							<?php if($linkedin) : ?>
							<li><a href="<?php echo $linkedin; ?>" target="_blank"><i class="icon icon-linkedin"></i></a></li>
							<?php endif; ?>
						</ul>
					</nav>
					<!-- .nav-socials -->
				</div>
				<!-- .container -->
			</div>
			<!-- .site__footer__top -->
			<div class="site-footer__bottom">
				<div class="container">
					<?php /* <div class="row">
						<div class="gr-1on5 gr-6@xs">
							<?php dynamic_sidebar('footer_1'); ?>
						</div>
						<!-- .gr -->
						<div class="gr-1on5 gr-6@xs">
							<?php dynamic_sidebar('footer_2'); ?>
						</div>
						<!-- .gr -->
						<div class="gr-1on5 gr-6@xs">
							<?php dynamic_sidebar('footer_3'); ?>
						</div>
						<!-- .gr -->
						<div class="gr-1on5 gr-6@xs">
							<?php dynamic_sidebar('footer_4'); ?>
						</div>
						<!-- .gr -->
						<div class="gr-1on5 gr-6@xs">
							<?php dynamic_sidebar('footer_5'); ?>
						</div>
						<!-- .gr -->
					</div>
					<!-- .row --> */ ?>
					<div class="site-footer__copyright">
						<p>Strona hostowana na Microsoft Azure dla Microsoft Sp. z o.o.</p>
						<div class="text-right hide@xs">
							<a href="https://www.microsoft.com/pl-pl" target="_blank">
								<img src="<?php echo THEME_URI; ?>/assets/images/logo.png" alt="" />
							</a>
						</div>
						<div class="row">
							<div class="gr-2">
								<span class="site-footer__copyright__language"><i class="icon icon-globus"></i>Polski (Polska)</span>
							</div>
							<!-- .gr -->
							<div class="gr-10">
								<nav class="nav--footer pull-right">
									<ul>
										<?php
											wp_nav_menu(
												array(
													'container'			=> "",
													'container_class'	=> "",
													'container_id'		=> "",
													'fallback_cb'		=> false,
													'menu_class'		=> "",
													'before'			=> '',
													'after'				=> '',
													'link_before'		=> '',
													'link_after'		=> '',
													'items_wrap'		=> '%3$s',
													'theme_location'	=> "footer",
													'walker'			=> '',
												)
											);
										?>
										<li>
											<a href="https://www.microsoft.com/pl-pl" target="_blank">
												<img src="<?php echo THEME_URI; ?>/assets/images/logo.png" alt="" class="show-inline@xxs mt-lg" />
												© <?php echo date('Y'); ?> Microsoft
											</a>
										</li>
									</ul>
								</nav>
							</div>
							<!-- .gr -->
						</div>
						<!-- .row -->
					</div>
					<!-- .copyright -->
				</div>
				<!-- .container -->
			</div>
			<!-- .site__footer__bottom -->
		</footer>
		<!-- .site-footer -->

	</div>
	<!-- #page -->

	<?php wp_footer(); ?>
	<script type="text/JavaScript">
    		var varSegmentation = 0;
   		var varClickTracking = 1;
    		var varCustomerTracking = 1;
   	 	var varAutoFirePV = 1;
    		var Route = "58500";
    		var Ctrl = "";
    		document.write("<script type='text/javascript' src='" + (window.location.protocol) + "//c.microsoft.com/ms.js'" + "'><\/script>");
	</script>
	<noscript><img alt="trans_pixel" width="1" height="1" src="https://c.microsoft.com/trans_pixel.aspx"/></noscript>
	
	<?php if(is_page_template( 'page-templates/template-contact.php' )) : ?>

	<?php endif; ?>
	</body>
</html>