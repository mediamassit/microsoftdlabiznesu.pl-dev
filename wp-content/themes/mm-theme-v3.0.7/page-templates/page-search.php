<?php

/**
 * Template Name: Wyszukiwarka
 */

global $wp_query;

get_header();

if(strlen(trim(@$_GET['q'])) >= 3) :
	$exclude = (array) get_field('dmx_exclude_from_search', 'options');
	$wp_query = new WP_Query(array(
		'post_type' => array('post', 'page', 'dx_event', 'dx_news', 'dx_product', 'dx_producer', 'dx_producer_post', 'dx_company', 'dx_designer', 'dx_developer', 'dx_arrangement'),
		's' => $_GET['q'],
		'paged' => get_query_var('paged', 1),
		'post__not_in' => $exclude,
		//'meta_query' => array(array('key' => '_thumbnail_id'))
	));

?>

		<?php get_template_part_args('partials/page_top'); ?>

			<?php if ( have_posts() ) : ?>
				<h2><?php _e('Wyniki wyszukiwania:', 'dx-theme'); ?> <?php echo esc_html(@$_GET['q']); ?></h2>

				<?php
					while ( have_posts() ) : the_post();
						$featured_image = @wp_get_attachment_image_src(@get_post_thumbnail_id(), 'thumbnail');
				?>
				<?php if($featured_image) : ?><figure><a href="<?php the_permalink(); ?>"><img src="<?php echo $featured_image[0]; ?>" alt="<?php the_title(); ?>"/></a></figure><?php endif; ?>
				
				<?php endwhile; ?>

								<?php
									$pagination = get_paginate_links(array(
										'format' => 'page/%#%',
										'current' => max(1, get_query_var('paged')),
										'total' => $wp_query->max_num_pages,
									));
									echo get_template_part_args('partials/pagination', array('pagination' => $pagination, 'max_pages' => $wp_query->max_num_pages));
								?>
			<?php else : ?>

			<?php endif; ?>

		<?php else : ?>


		<?php get_template_part_args('partials/page_top'); ?>

		
		<?php if(!isset($_GET['q'])) : ?>
		<h2><?php _e('Wyszukiwarka', 'dmx-theme'); ?></h2>
		<?php else : ?>
		<h2><?php _e('Fraza zbyt krótka', 'dmx-theme'); ?></h2>
		<p><?php _e('Poszukiwana fraza jest zbyt krótka, musi zawierać conajmniej 3 znaki.', 'dmx-theme'); ?></p>
		<?php endif; ?>
		
<?php endif; get_footer(); ?>