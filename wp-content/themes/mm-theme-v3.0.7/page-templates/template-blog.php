<?php
/**
 * Template Name: Wszystkie wpisy
 */
get_header();
$slider = get_field('home_slider');
?>

	<?php if(is_foreachable($slider)) : ?>
		<section class="section__slider">
			<div class="container">
				<div class="section__slider__wrapper">
					<?php foreach ($slider as $slide) : ?>
					<div class="section__slider__slide">
						<img src="<?php echo $slide['image']['url']; ?>" alt="" />
						<div class="section__slider__content bg--yellow">
							<h2><?php echo $slide['title']; ?></h2>
							<p><?php echo $slide['content']; ?></p>
						</div>
						<!-- .section__slider__content -->
					</div>
					<!-- .section__slider__slide -->
					<?php endforeach; ?>
				</div>
				<!-- .section__slider__wrapper -->
			</div>
			<!-- .contaienr -->
		</section>
		<!-- .page-section -->
	<?php endif; ?>

		<section class="page-section section__home">
			<div class="container">
				<div class="row">
					<div class="gr-8  gr-12@xs">
						<div class="section__posts section__posts--lists">
							<?php
							if ( have_posts() ) :
								while(have_posts()) : the_post();
							?>
								<? php get_template_part( 'content' ); ?>
							<?php
								endwhile;
							endif;
							?>
							<?php
								$pagination = get_paginate_links(array(
									'format' => 'page/%#%',
									'current' => max(1, get_query_var('paged')),
									'total' => $wp_query->max_num_pages,
								));
								echo get_template_part_args('partials/pagination', array('pagination' => $pagination, 'max_pages' => $wp_query->max_num_pages));
							?>
						</div>
						<!-- .section__posts -->
					</div>
					<!-- .gr -->
					<div class="gr-4 gr-12@xs">
						<div class="section__sidebar">
							<?php display_categories(get_the_id()); ?>
							<?php dynamic_sidebar('sidebar_default'); ?>
							<?php dynamic_sidebar('sidebar_single'); ?>
						</div>
						<!-- .section__sidebar -->
					</div>
					<!-- .gr -->
				</div>
				<!-- .row -->
			</div>
			<!-- .container -->
		</section>

		<?php if($archive_tabs) : ?>
		<section class="section__tabs">
			<div class="container">
				<div class="section__tabs__wrapper">
					<div class="row">
						<div class="gr-3 gr-12@xs">
							<nav class="section__tabs__nav js-nav-tabs">
								<ul>
								<?php foreach($archive_tabs as $i => $tab) : ?>
									<li><a href="#tab_<?php echo $i; ?>" <?php if($i == 0) echo 'class="active"'; ?>><?php echo $tab['title']; ?></a></li>
								<?php endforeach; ?>
								</ul>
							</nav>
						</div>
						<!-- .gr -->
						<div class="gr-9 gr-12@xs">
							<div class="section__tabs__tab">
								<?php foreach($archive_tabs as $i => $tab) : ?>
								<div class="section__tabs__single <?php if($i == 0) echo 'active'; ?>" id="tab_<?php echo $i; ?>" <?php if($tab): ?>style="background-image: url('<?php echo $tab['image']['sizes']['thumb-900x500']; ?>');"<?php endif; ?>>
									<?php if($tab['content']) : ?>
									<div class="section__tabs__content bg--yellow">
										<?php echo $tab['content']; ?>
									</div>
									<!-- .section__tabs__content -->
									<?php endif; ?>
								</div>
								<!-- .section__tabs__single -->
								<?php endforeach; ?>
							</div>
							<!-- section__tabs__tab -->
						</div>
						<!-- .gr -->
					</div>
					<!-- .row -->
				</div>
				<!-- .section__tabs__wrapper -->
			</div>
			<!-- .contaienr -->
		</section>
		<!-- .section__tabs -->
		<?php endif; ?>
<?php

get_footer(); 
?>