<?php
/**
 * Default widget template.
 *
 * Copy this template to /simple-image-widget/widget.php in your theme or
 * child theme to make edits.
 *
 * @package   SimpleImageWidget
 * @copyright Copyright (c) 2015 Cedaro, LLC
 * @license   GPL-2.0+
 * @since     4.0.0
 */
?>
<div class="widget--cta <?php if ( ! empty( $image_id ) ) : ?>widget--cta--img<?php else: ?>widget--cta--blue<?php endif; ?>">
	<a href="<?php echo $link; ?>" target="_blank">
		<?php if ( ! empty( $image_id ) ) : ?>
			<?php
				echo wp_get_attachment_image( $image_id, $image_size );
				$alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);
			?>
		<?php endif; ?>
		<?php if($title) : ?>
		<h4><?php echo $title; ?></h4>
		<?php endif; ?>
		<?php if($text) : ?>
		<?php echo wpautop( $text ); ?>
		<?php endif; ?>
		<?php if($link_text) : ?>
		<span class="widget--cta__more"><?php echo $link_text; ?></span>
		<?php endif; ?>
	</a>
</div>
<!-- .widget -->