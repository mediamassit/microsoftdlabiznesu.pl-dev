<?php
get_header();
$slider = get_field('home_slider', get_option('page_for_posts'));

?>

	<?php if(is_foreachable($slider)) : ?>
		<section class="section__slider">
			<div class="container">
				<div class="section__slider__wrapper">
					<?php foreach ($slider as $slide) : ?>
					<div class="section__slider__slide">
						<img src="<?php echo $slide['image']['url']; ?>" alt="" />
						<div class="section__slider__content">
							<h2><?php echo $slide['title']; ?></h2>
							<p><?php echo $slide['content']; ?></p>
						</div>
						<!-- .section__slider__content -->
					</div>
					<!-- .section__slider__slide -->
					<?php endforeach; ?>
				</div>
				<!-- .section__slider__wrapper -->
			</div>
			<!-- .contaienr -->
		</section>
		<!-- .page-section -->
	<?php endif; ?>

	<?php if ( have_posts() ) : ?>
		<section class="page-section section__home">
			<div class="container">
				<div class="row">
					<div class="gr-8  gr-12@xs">
						<div class="section__posts section__posts--lists">
							<?php
								while(have_posts()) : the_post();
							?>
								<?php get_template_part( 'content' ); ?>
							<?php
								endwhile;
							?>
							<?php
								$pagination = get_paginate_links(array(
									'format' => 'page/%#%',
									'current' => max(1, get_query_var('paged')),
									'total' => $wp_query->max_num_pages,
								));
								echo get_template_part_args('partials/pagination', array('pagination' => $pagination, 'max_pages' => $wp_query->max_num_pages));
							?>
						</div>
						<!-- .section__posts -->
					</div>
					<!-- .gr -->
					<div class="gr-4 gr-12@xs">
						<div class="section__sidebar">
							<?php display_categories(get_the_id()); ?>
							<?php dynamic_sidebar('sidebar_default'); ?>
							<?php dynamic_sidebar('sidebar_single'); ?>
						</div>
						<!-- .section__sidebar -->
					</div>
					<!-- .gr -->
				</div>
				<!-- .row -->
			</div>
			<!-- .container -->
		</section>
	<?php else: ?>
		<div class="page__title">
			<div class="container">
				<h1><?php _e('404', THEME_NAME); ?></h1>
			</div>
			<!-- .container -->
		</div>

		<section class="page-section section__single mt-none">
			<div class="container">
				<div class="row">
					<div class="gr-8 gr-12@xs">
						<div class="section__content">
							<p><?php _e('Wskazana podstrona nie istnieje. Wróć do ', THEME_NAME); ?><a href="/"><?php _e('strony głównej',THEME_NAME); ?></a>.</p>
						</div>
						<!-- .section__content -->
					</div>
					<!-- .gr -->
					<div class="gr-4 gr-12@xs">
						<div class="section__sidebar">
						</div>
						<!-- .section__sidebar -->
					</div>
					<!-- .gr -->
				</div>
				<!-- .row -->
			</div>
			<!-- .container -->
		</section>
		<!-- .section__single -->
	<?php endif; ?>
	

<?php

get_footer(); 
?>