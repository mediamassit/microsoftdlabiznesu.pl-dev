<?php
get_header();
 $btn = get_field('chose_btn') ;
$link_azure = get_field('link_azure', option);
$link_ofice = get_field('link_ofice', option);

while(have_posts()) : the_post();
?>
		<div class="page__title">
			<div class="container">
				<h1><?php the_title(); ?></h1>
			</div>
			<!-- .container -->
		</div>
		
		<div class="page-section section__single mt-none">
			<div class="container">
				<div class="row">
					<div class="gr-8 gr-12@xs">
						<div class="section__single__thumbnail">
						<?php if(has_post_thumbnail()) : ?>
							<?php the_post_thumbnail('full'); ?>
						<?php endif; ?>
						<?php /* <h5><?php the_time('j F Y');?></h5> */ ?>
						</div>
						<div class="section__content">
						<?php
							$content = get_the_content();
							$content = apply_filters('the_content', $content);
							if(!$content) {
						?>
								<p><?php _e('Brak treści.',THEME_NAME); ?></p>
						<?php 
							} else {
								echo $content;
							}
						?>
						</div>
						<!-- .section__content -->
					</div>
					<!-- .gr -->
					<div class="gr-4 gr-12@xs">
						<div class="section__sidebar">
							<?php display_categories(get_the_id(),true); ?>
							<?php dynamic_sidebar('sidebar_default'); ?>
							<?php dynamic_sidebar('sidebar_single'); ?>
						</div>
						<!-- .section__sidebar -->
					</div>
					<!-- .gr -->
				</div>
				<!-- .row -->
			</div>
			<!-- .container -->
		</div>
		<!-- .section__single -->

		<?php
		$args = array(
			'post_type' 		=> 'post',
			'orderby '			=> 'date',
			'order  '			=> 'ASC',
			'posts_per_page'	=> '6',
			'cat'			    => 14
		);
		$news = new WP_Query($args);
		$i = 0;
		if ( $news->have_posts() ) :
		?>
		<section class="section__posts section__posts--last">
			<div class="container">
				<div class="section__posts__wrapper">
					<h3><?php _e('Nasze rozwiązania w praktyce',THEME_NAME); ?></h3>
					<div class="row">
					<?php while($news->have_posts()) : $news->the_post(); ?>
						<?php if($i == 3) echo '</div><div class="row">'; ?>
						<div class="gr-4 gr-6@sm">
						<?php get_template_part( 'content','tile' ); ?>
						</div>
						<?php $i++; ?>
					<?php endwhile; ?>
					</div>
				</div>
				<!-- .section__posts__wrapper -->
			</div>
			<!-- .container -->
		</section>
		<!-- .section__posts section__posts -->
		<?php endif; ?>

<?php if($btn): 
$i=0;
foreach($btn as $button) : ;?>
<div class="buttons-link">
	<?php if($button == 'Azure'): ;?>
	<div class="btn-link">
		<a href="<?php if($link_azure): echo $link_azure; else: echo '#' ; endif ;?>" target="_blank">
			<p>WYPRÓBUJ AZURE</p>
		</a>
	</div>
	<? $i++ ; endif ;
	if($button == 'Office 365'):
	;?>
	
	<div class="btn-link-second" <?php if($i==0): echo ' style="top:225px;"' ;endif;?> >
		<a href="<?php  if($link_ofice): echo $link_ofice; else: echo '#' ; endif ;?>" target="_blank">
			<p>WYPRÓBUJ OFFICE 365</p>
		</a>
	</div>
	<?php endif; ?>
</div>
<?php endforeach; endif; ?>


<?php
endwhile;

get_footer(); 
?>