<?php 

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_5891f85a03402',
	'title' => 'Przycisk',
	'fields' => array (
		array (
			'key' => 'field_5891f8b60be87',
			'label' => 'Wybierz przycisk',
			'name' => 'chose_btn',
			'type' => 'checkbox',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'Azure' => 'Azure',
				'Office 365' => 'Office 365',
			),
			'default_value' => array (
			),
			'layout' => 'vertical',
			'toggle' => 0,
		),

	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;



