<?php
/*** Post types ***/
//require 'post-type/testimonial.php';

/*** Taxonomies ***/
//require 'taxonomy/';

/*** Fields ***/
require 'custom-field/home.php';
require 'custom-field/archive.php';
require 'custom-field/post.php';
require 'taxonomy/post_craft.php';
require 'taxonomy/post_advantage.php';

require 'settings/website.php';