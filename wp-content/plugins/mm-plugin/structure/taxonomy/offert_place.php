<?php

function mm_taxonomy_place() {
	$labels = array(
		'name'              => _x( 'Miejscowości', '' ),
		'singular_name'     => _x( 'Miejscowość oferty', '' ),
		'search_items'      => __( 'Szukaj' ),
		'all_items'         => __( 'Wszystkie miejscowości' ),
		'parent_item'       => __( 'Rodzic' ),
		'parent_item_colon' => __( 'Rodzic:' ),
		'edit_item'         => __( 'Edytuj' ),
		'update_item'       => __( 'Zaaktualizuj' ),
		'add_new_item'      => __( 'Dodaj nową miejscowość' ),
		'new_item_name'     => __( 'Nowa miejscowość' ),
		'menu_name'         => __( 'Miejscowości' ),
	);
	$rewrite = array(
		'slug'                       => 'miejscowosc',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => $rewrite
	);
	register_taxonomy( 'offert_place',  array('offert'), $args );
}
add_action( 'init', 'mm_taxonomy_place', 0 );