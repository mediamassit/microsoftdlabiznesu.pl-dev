<?php

function mm_taxonomy_post_advantage() {
	$labels = array(
		'name'              => _x( 'Rezultaty', '' ),
		'singular_name'     => _x( 'Rezultaty', '' ),
		'search_items'      => __( 'Szukaj' ),
		'all_items'         => __( 'Wszystkie rezultaty' ),
		'parent_item'       => __( 'Rodzic' ),
		'parent_item_colon' => __( 'Rodzic:' ),
		'edit_item'         => __( 'Edytuj' ),
		'update_item'       => __( 'Zaaktualizuj' ),
		'add_new_item'      => __( 'Dodaj nowy' ),
		'new_item_name'     => __( 'Nowy rezultat' ),
		'menu_name'         => __( 'Rezultaty' ),
	);
	$rewrite = array(
		'slug'                       => 'rezultaty',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => $rewrite
	);
	register_taxonomy( 'post_advantage',  array('post'), $args );
}
add_action( 'init', 'mm_taxonomy_post_advantage', 0 );