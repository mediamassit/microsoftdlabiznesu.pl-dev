<?php

function mm_taxonomy_offert_craft() {
	$labels = array(
		'name'              => _x( 'Braża', '' ),
		'singular_name'     => _x( 'Braża', '' ),
		'search_items'      => __( 'Szukaj' ),
		'all_items'         => __( 'Wszystkie' ),
		'parent_item'       => __( 'Rodzic' ),
		'parent_item_colon' => __( 'Rodzic:' ),
		'edit_item'         => __( 'Edytuj' ),
		'update_item'       => __( 'Zaaktualizuj' ),
		'add_new_item'      => __( 'Dodaj nową branżę' ),
		'new_item_name'     => __( 'Nowa branża' ),
		'menu_name'         => __( 'Branże' ),
	);
	$rewrite = array(
		'slug'                       => 'branza',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => $rewrite
	);
	register_taxonomy( 'post_craft',  array('post'), $args );
}
add_action( 'init', 'mm_taxonomy_offert_craft', 0 );